from conans import CMake, ConanFile, tools


class XtlConan(ConanFile):
    name = "xtl"
    version = "0.6.4"
    license = "BSD3"
    url = "https://gitlab.com/mdavezac-conan/xtl"
    homepage = "https://github.com/QuantStack/xtl"
    description = "n-dimensional arrays with broadcasting and lazy computing."
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake"
    no_copy_source = True

    def source(self):
        git = tools.Git(folder="xtl")
        git.clone(self.homepage, branch=self.version)

    def configured_cmake(self):
        cmake = CMake(self)
        cmake.configure(source_folder="xtl")
        return cmake

    def build(self):
        return self.configured_cmake().build()

    def package(self):
        return self.configured_cmake().install()
